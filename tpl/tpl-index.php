<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?= SITE_TITLE ?></title>
    <link rel="stylesheet" href="<?= BASE_URL ?>assets/css/style.css">

</head>
<body>
<div class="page">
    <div class="pageHeader">
        <div class="title">Dashboard</div>
        <div class="userPanel">
            <a href="<?= site_url("?logout=1") ?>"><i class="fa fa-sign-out"></i></a>

            <span class="username"><?= $user->name ?? 'Unknown'; ?> </span>
            <img src="<?= $user->image; ?>" width="40" height="40" alt=""/>

        </div>
    </div>
    <div class="main">
        <div class="nav">
            <div class="searchbox">
                <label><i class="fa fa-search"></i>
                    <input type="search" placeholder="Search"/>
                </label>
            </div>
            <div class="menu">
                <div class="title">FOLDERS</div>
                <ul class="folder_list">
                    <li class="<?= isset($_GET['folder_id']) ? '' : 'active' ?>">
                        <a herf="<?= site_url() ?>"><i class="fa fa-folder"></i>All
                    </li>
                    <?php
                    foreach ($folders as $folder): ?>
                        <li class="<?= (isset($_GET['folder_id']) && $_GET['folder_id'] == $folder->id) ? 'active' : '' ?>">
                            <a href="?folder_id=<?= $folder->id ?>"><i class="fa fa-folder"></i><?= $folder->name ?>
                            </a>
                            <a href="?delete_folder=<?= $folder->id ?>"><i class="button inverse"><i
                                            class="fa fa-trash-o"
                                            onclick="return confirm('Are You Sure To Delete This Item?\n<?= $folder->name ?>');"
                                            style="background-color: inherit; width: 10px; color: #AAA; font-size: 1.3em; padding-left: 10px; margin-left: 25px;"></i></a>
                        </li>
                    <?php
                    endforeach; ?>
                </ul>
            </div>
            <div>
                <input type="text" id="addFolderInput" style="width: 65%;margin:3%" placeholder="Add New Folder">
                <button id="addFolderBtn" class="btn ">+</button>
            </div>
        </div>
        <div class="view">
            <div class="viewHeader">
                <div class="title" style='width: 50%'>
                    <input type="text" id="TaskNameInput" style='width: 100%;margin:3%;line-height: 25px'
                           placeholder="Add New Task"/>
                </div>
                <div class="functions">
                    <div class="button active new-task">Add New Task</div>
                    <div class="button">Completed</div>
                    <div class="button inverz"><i class="fa fa-trash-o"></i></div>
                </div>
            </div>
            <div class="content">
                <div class="list">
                    <div class="title">Today</div>
                    <ul>
                        <?php
                        if (sizeof($tasks)): ?>
                            <?php
                            foreach ($tasks as $task): ?>
                                <li class="<?= $task->is_done ? 'checked' : ''; ?>">
                                    <i data-taskId="<?= $task->id ?>"
                                       class="isDone clickable fa <?= $task->is_done ? 'fa-check-square-o' : 'fa-square-o'; ?>"></i>
                                    <span><?= $task->title ?></span>
                                    <div class="info">
                                        <span class="created-at">Created At <?= $task->created_at ?></span><a
                                                href="?delete_task= <?= $task->id ?>" class="fa fa-trash-o"
                                                onclick="return confirm('Are You Sure To Delete This Item? \n <?= $task->title ?>');"
                                                style="background-color: inherit;  width: 10px; color: #AAA; font-size: 1.3em; padding-left: 25px; padding-right: 25px; margin: 15px; "></a>
                                    </div>
                                </li>
                            <?php
                            endforeach; ?>
                        <?php
                        else: ?>
                            <li>No Task Here ..</li>
                        <?php
                        endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- partial -->
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script src="<?= BASE_URL ?>assets/js/script.js"></script>
<script>
    $(document).ready(function () {
        $('.new-task').click(function (e) {
            addTask();
        })

        $('.isDone').click(function (e) {
            var tid = $(this).attr('data-taskId');
            // alert(tid);
            $.ajax({
                url: "process/ajaxHandler.php",
                method: "post",
                data: {action: "doneSwitch", taskId:tid},
                success: function (response) {
                    // alert('The success codes have been executed properly.');
                    if (response == '1') {
                         location.reload();

                    }

                }

            });
        })

        $('#addFolderBtn').click(function (e) {
            var input = $('input#addFolderInput');
            if (input.val().length < 3) {
                alert('نام فولدر باید بزرگتر از 2 حرف باشد');
                return;
            }

            $.ajax({
                url: "process/ajaxHandler.php",
                method: "post",
                data: {action: "addFolder", folderName: input.val()},
                success: function (response) {
                    if (response == 1) {
                        $('<li> <a href="#"><i class="fa fa-folder"></i>' + input.val() + '</a></li>').appendTo('ul.folder_list');
                        input.val('');
                    } else {
                        alert(response);
                    }
                },
                error: function (jqXhr, textStatus, errorMessage) { // error callback
                    alert('Error: ' + errorMessage);
                }
            });

        });

        $('#TaskNameInput').on('keypress', function (e) {
            if (e.which == 13) {
                addTask();
            }
        });

    });

    function addTask() {
        const folderId = <?= $_GET['folder_id'] ?? 'undefined' ?>;
        const taskTitle = $('#TaskNameInput').val();
        $('#TaskNameInput').focus();
        if (folderId === undefined) {
            alert('Folder ID is empty!');
            return;
        }

        if (taskTitle.length < 3) {
            alert('عنوان تسک باید بزرگتر از 2 حرف باشد');
            return;
        }

        $.ajax({
            url: "process/ajaxHandler.php",
            method: "post",
            data: {action: "addTask", folderId: folderId, taskTitle},
            success: function (response) {
                if (response == 1) {
                    location.reload();
                    return;
                }

                alert(response);
            },
            error: function (jqXhr, textStatus, errorMessage) { // error callback
                alert('Error: ' + errorMessage);
            }
        });


    }
</script>

</body>
</html>