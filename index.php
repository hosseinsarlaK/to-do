<?php
include "bootstrap/init.php";

if (isset($_GET['logout'])){
    logout();
}

if (!isLoggedIn()){
    // redirect to aut form
    redirect(site_url('auth.php'));
}
# user is loggedIn
$user = getLoggedInUser();

# connect to db and get tasks
if(isset($_GET['delete_folder']) && is_numeric($_GET['delete_folder'])){
    $deletedCount = deleteFolder($_GET['delete_folder']);
//    echo "$deletedCount folders successfully deleted!";
}

if(isset($_GET['delete_task']) && is_numeric($_GET['delete_task'])) {
    $deletedCount = deleteTask($_GET['delete_task']);
//    echo "$deletedCount tasks successfully deleted!";
}
# connect to db and get tasks
$folders = getFolders();

$tasks = getTasks();
//dd($tasks);
//var_dump($tasks);

include "tpl/tpl-index.php";