<?php
include_once "../bootstrap/init.php";

if (!isAjaxRequest()) {
    die("Invalid Request!");
}

$action = $_POST['action'] ?? NULL;


switch ($action) {
    case "doneSwitch";
        $task_id = $_POST['taskId'];
        if (!isset($task_id) || !is_numeric($task_id)) {
            die("ایدی تسک معتبر نیست");
        }
        doneSwitch($task_id);


        break;
    case "addFolder":
        $folderName = $_POST['folderName'] ?? NULL;
        if (empty($folderName)) {
            die("نام فولدر نمیتواند خالی باشد");
        }

        if (strlen($folderName) < 3) {
            die("نام فولدر باید بزرگتر از 2 حرف باشد");
        }
        echo addFolder($folderName);
        die;

    case "addTask":
        $folderId = $_POST['folderId'] ?? NULL;
        $taskTitle = $_POST['taskTitle'] ?? NULL;
        if (empty($folderId)) {
            die("فولدر را انتخاب کنید");
        }

        if (empty($taskTitle)) {
            die("عنوان تسک نمیتواند خالی باشد");
        }

        if (strlen($taskTitle) < 3) {
            die("عنوان تسک باید بزرگتر از 2 حرف باشد");
        }
        echo addTask($folderId, $taskTitle);
        die;

    default:
        die("Invalid Action!");
}