<?php defined('BASE_PATH') OR die("Permission Denied!");
/*** Auth Functions ***/


/*** get login user id***/
function getCurrentUserId(): int
{
    return getLoggedInUser()->id ?? 0;
}

function isLoggedIn(): bool
{
    return isset($_SESSION['login']);
}

function getLoggedInUser()
{

    return $_SESSION['login'] ?? 'null';

}

function getUserByEmil($email)
{

    global $pdo;
    $sql = "SELECT * FROM `users` WHERE email = :email";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':email'=> $email]);
    $records = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $records[0] ?? null;

}

function logout()
{
    unset($_SESSION['login']);
}

function login($email,$pass): int
{
    $user = getUserByEmil($email);

    if(is_null($user)){
        return false;
    }

    #check the password
    if (password_verify($pass,$user->password)){
        # login is successfully
        $user->image = $grav_url = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $user->password ) ) );
        $_SESSION['login'] = $user;

        return true;
    }
    return false;
}

function register($userData): int
{
    global $pdo;
    # validation of $userData here (isValidEmail, isValidUserName , isValidPassword)
    $passHash = password_hash($userData['password'], PASSWORD_BCRYPT);
    $sql = "INSERT INTO `users`(name,email,password) VALUES (:name , :email ,:pass);";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([':name'=>$userData['name'] , ':email'=>$userData['email'] , ':pass'=>$passHash]);
    return (bool)$stmt->rowCount();
}

