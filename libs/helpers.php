<?php use JetBrains\PhpStorm\NoReturn;

defined('BASE_PATH') OR die("Permission Denied!");
function getCurrentUrl(): int
{
    return 1;
}

function isAjaxRequest(): bool
{
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
        return true;
        //request is ajax
    }
    return false;
}

function site_url($uri = ''): string
{
    return BASE_URL . $uri;
}

function redirect($url)
{
    header("Location:  $url");
    die();
}


function diePage($msg): void
{
    echo "<div style='padding: 30px; width: 80%; margin: 50px auto; background: #f9dede; border: 1px solid; border-radius: 5px; font-size: 24px;'>$msg</div>";
    die();
}

#[NoReturn] function message($msg,$cssClass= 'info'): void
{
    echo "<div class='$cssClass' style='padding: 20px; width: 80%; margin: 50px auto; background: #f9dede; border: 1px solid; border-radius: 5px; font-size: 24px;'>$msg</div>";
    die();
}

function dd($var): void
{
    echo "<pre style='color: #1c0404; background: #fff; z-index: 999; position: relative; padding: 10px; margin: 10px; border-radius: 5px; border-left: 3px solid #462929;'>";
    var_dump($var);
    echo "</pre>";
}

